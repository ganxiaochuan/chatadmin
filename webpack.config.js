var path = require('path');
var webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

// const IS_BUILD = process.env.NODE_ENV === 'production';
const IS_BUILD = true;
// const OUTPUT = './public/';

var fileLoaderOption = {
    loader: 'file-loader',
    options: {
        name: '[path][name].[ext]',
        outputPath: function(path) {
            console.log(path);
            return path.replace(/^src/g, ".");
        }
    }
};

var css = {
    loader: "css-loader",
    options: {
        url: false
    }
}

module.exports = {
    entry: {
        main: './src/main.js',
        index:'./src/index.js',
        login:'./src/login.js'
    },
    output: {
        path: path.resolve(__dirname, './public'),
        publicPath: '/public/',
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loaders: {},
                extractCSS: IS_BUILD
                    // other vue-loader options go here
            }
        }, {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.less$/,
            use: !!!IS_BUILD ? ['style-loader', 'css-loader', 'less-loader'] : ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [css, "less-loader"]
            })
        }, {
            test: /\.css$/,
            use: !!!IS_BUILD ? ['style-loader', 'css-loader'] : ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [css]
            })
        }, {
            test: /\.(woff|woff2|eot|ttf|otf|png|jpg|gif|svg)$/,
            use: [fileLoaderOption]
        }]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    plugins: [
        new ExtractTextPlugin(`/css/[name].css`),
    ]
}